﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing;
using System.Data.OleDb;

namespace Avatar
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        MainWindow aux;
        String[] aux1 = new String[13];


        public Window1(MainWindow aux)
        {
            InitializeComponent();
            this.aux = aux;
            LeerDatosBDPRecios();
            String PeloSimas = "10,000";
            String PeloNegro = "5,000";
            String PeloRojoAlargado = "5,000";
            String PeinadoCuadrado = "4,000";
            String PeinadoEstiloAbuelo = "1,000";
            String PeloAfroRubio ="1,000";
            String CamisetaRoja = "5,000";
            String CamisetaRosada = "10,000";
            String CamisetaMarron = "1,000";
            String PantalonesAzules = "15,000";
            String PantalonesAmarillo = "15,000";
            String SerBlanco = "20,000";
            aux1[1] = PeloSimas;
            aux1[2] = PeloNegro;
            aux1[3] = PeloRojoAlargado;
            aux1[4] = PeinadoCuadrado;
            aux1[5] = PeinadoEstiloAbuelo;
            aux1[6] = PeloAfroRubio;
            aux1[7] = CamisetaRoja;
            aux1[8] = CamisetaRosada;
            aux1[9] = CamisetaMarron;
            aux1[10] = PantalonesAzules;
            aux1[11] = PantalonesAmarillo;
            aux1[12] = SerBlanco;

        }
        
        private void BotonComprar_Click(object sender, RoutedEventArgs e)
        {   
            //////////////////////////////////////////////////PEINADOS:///////////////////////////////////////////
            VolverPeloNormal();
            EscribirBBDDPRecios();
            if (SelectPeinado1.IsChecked == true) {
                int aux1 = Convert.ToInt32(PrecioPElo.Content.ToString().Replace(",", ""));

                if (aux.getDinero() >= aux1) 
                {
                    MessageBox.Show("Su compra se ha realizado con exito ");
                    int aux2 = aux.getDinero();
                    aux.setDinero(aux2 - aux1);
                    aux.elPelo1.Height = 264.835;
                    aux.elPelo1.Fill = Brushes.DarkBlue;
                    aux.CambiarTop(-110.94);
                    PrecioPElo.Content = 0;


                }
                else {
                    MessageBox.Show("No tiene Suficiente Dinero");
                }

            }
            if (PeloAlargado1.IsChecked == true)
            {
                int aux1 = Convert.ToInt32(PrecioPElo_Copy2.Content.ToString().Replace(",", ""));

                if (aux.getDinero() >= aux1)
                {
                    MessageBox.Show("Su compra se ha realizado con exito ");
                    int aux2 = aux.getDinero();
                    aux.setDinero(aux2 - aux1);
                    aux.elPelo1.Height = 264.835;
                    aux.elPelo1.Fill = Brushes.Black;
                    aux.CambiarTop(-110.94);
                    PrecioPElo_Copy2.Content = 0;

                }
                else
                {
                    MessageBox.Show("No tiene Suficiente Dinero");
                }

            }
            if (PeloAlargadoRojo.IsChecked == true)
            {
                int aux1 = Convert.ToInt32(PrecioPElo_Copy5.Content.ToString().Replace(",", ""));

                if (aux.getDinero() >= aux1)
                {
                    MessageBox.Show("Su compra se ha realizado con exito ");
                    int aux2 = aux.getDinero();
                    aux.setDinero(aux2 - aux1);
                    aux.elPelo1.Height = 264.835;
                    aux.elPelo1.Fill = Brushes.Red;
                    aux.CambiarTop(-110.94);
                    PrecioPElo_Copy5.Content = 0;

                }
                else
                {
                    MessageBox.Show("No tiene Suficiente Dinero");
                }

            }
            if (SelectorPeinado2.IsChecked == true){
                int aux1 = Convert.ToInt32(PrecioPElo_Copy.Content.ToString().Replace(",", ""));

                if (aux.getDinero() >= aux1){
                    MessageBox.Show("Su compra se ha realizado con exito ");
                    int aux2 = aux.getDinero();
                    aux.setDinero(aux2 - aux1);
                    aux.elPelo1.Width = 252.691;
                    aux.elPelo1.Fill = Brushes.Black;
                    aux.CambiarLeft(-48.524);
                    PrecioPElo_Copy.Content = 0;

                }
                else
                {
                    MessageBox.Show("No tiene Suficiente Dinero");
                }

            }
            if (Selectorafrocorto.IsChecked == true)
            {
                int aux1 = Convert.ToInt32(PrecioPElo_Copy1.Content.ToString().Replace(",", ""));

                if (aux.getDinero() >= aux1)
                {
                    MessageBox.Show("Su compra se ha realizado con exito ");
                    int aux2 = aux.getDinero();
                    aux.setDinero(aux2 - aux1);
                    aux.elPelo1.Height = 75.261;
                    aux.elPelo1.Fill = Brushes.Black;
                    aux.CambiarTop(71.406);
                    PrecioPElo_Copy1.Content = 0;


                }
                else
                {
                    MessageBox.Show("No tiene Suficiente Dinero");
                }

            }
            if (SelectAfroRubio.IsChecked == true)
            {
                int aux1 = Convert.ToInt32(PrecioPElo_Copy3.Content.ToString().Replace(",", ""));

                if (aux.getDinero() >= aux1)
                {
                    MessageBox.Show("Su compra se ha realizado con exito ");
                    int aux2 = aux.getDinero();
                    aux.setDinero(aux2 - aux1);
                    aux.elPelo1.Fill = Brushes.Yellow;
                    PrecioPElo_Copy3.Content = 0;

                }
                else
                {
                    MessageBox.Show("No tiene Suficiente Dinero");
                }
            }
            /////////////////////////////////////CAMISETAS:////////////////////////////////////
            if (CamisetaMarron.IsChecked == true){
                int aux1 = Convert.ToInt32(PrecioPElo_Copy4.Content.ToString().Replace(",", ""));
                if (aux.getDinero() >= aux1){
                    MessageBox.Show("Su compra se ha realizado con exito ");
                    int aux2 = aux.getDinero();
                    aux.setDinero(aux2 - aux1);
                    aux.barriga.Fill = Brushes.SaddleBrown;
                    aux.camiseta1.Fill = Brushes.SaddleBrown;
                    aux.camiseta2.Fill = Brushes.SaddleBrown;
                    aux.ParteCamiseta.Fill = Brushes.SaddleBrown;
                    aux.ParteCamiseta1.Fill = Brushes.SaddleBrown;
                    PrecioPElo_Copy4.Content = 0;
                }
                else{
                    MessageBox.Show("No tiene Suficiente Dinero");
                }
            }
            if (CamisetaRoja.IsChecked == true){
                int aux1 = Convert.ToInt32(PrecioPElo_Copy9.Content.ToString().Replace(",", ""));
                if (aux.getDinero() >= aux1)
                {
                    MessageBox.Show("Su compra se ha realizado con exito ");
                    int aux2 = aux.getDinero();
                    aux.setDinero(aux2 - aux1);
                    aux.barriga.Fill = Brushes.Red;
                    aux.camiseta1.Fill = Brushes.Red;
                    aux.camiseta2.Fill = Brushes.Red;
                    aux.ParteCamiseta.Fill = Brushes.Red;
                    aux.ParteCamiseta1.Fill = Brushes.Red;
                    PrecioPElo_Copy9.Content = 0;
                }
                else{
                    MessageBox.Show("No tiene Suficiente Dinero");
                }
            }
            if (CamisetaRosado.IsChecked == true){
                int aux1 = Convert.ToInt32(PrecioPElo_Copy6.Content.ToString().Replace(",", ""));
                if (aux.getDinero() >= aux1)
                {
                    MessageBox.Show("Su compra se ha realizado con exito ");
                    int aux2 = aux.getDinero();
                    aux.setDinero(aux2 - aux1);
                    aux.barriga.Fill = Brushes.HotPink;
                    aux.camiseta1.Fill = Brushes.HotPink;
                    aux.camiseta2.Fill = Brushes.HotPink;
                    aux.ParteCamiseta.Fill = Brushes.HotPink;
                    aux.ParteCamiseta1.Fill = Brushes.HotPink;
                    PrecioPElo_Copy6.Content = 0;

                }
                else
                {
                    MessageBox.Show("No tiene Suficiente Dinero");
                }
            }
            /////////////////// PANTALONES ////////////////////////////////////////
            if (PantalonesAzulOscu.IsChecked == true){
                int aux1 = Convert.ToInt32(PrecioPElo_Copy7.Content.ToString().Replace(",", ""));
                if (aux.getDinero() >= aux1)
                {
                    MessageBox.Show("Su compra se ha realizado con exito ");
                    int aux2 = aux.getDinero();
                    aux.setDinero(aux2 - aux1);
                    aux.pantalones.Fill = Brushes.DarkBlue;
                    PrecioPElo_Copy7.Content = 0;

                }
                else
                {
                    MessageBox.Show("No tiene Suficiente Dinero");
                }
            }
            if (PantalonesAmarillos.IsChecked == true){
                int aux1 = Convert.ToInt32(PrecioPElo_Copy8.Content.ToString().Replace(",", ""));
                if (aux.getDinero() >= aux1)
                {
                    MessageBox.Show("Su compra se ha realizado con exito ");
                    int aux2 = aux.getDinero();
                    aux.setDinero(aux2 - aux1);
                    aux.pantalones.Fill = Brushes.Yellow;
                    PrecioPElo_Copy8.Content = 0;

                }
                else
                {
                    MessageBox.Show("No tiene Suficiente Dinero");
                }
            }
            ///////////////////////////Ser Blanco//////////////////////
            if (SerBlanco.IsChecked == true)
            {
                int aux1 = Convert.ToInt32(PrecioPElo_Copy10.Content.ToString().Replace(",", ""));
                if (aux.getDinero() >= aux1)
                {
                    MessageBox.Show("Su compra se ha realizado con exito ");
                    int aux2 = aux.getDinero();
                    aux.setDinero(aux2 - aux1);
                    //La cara:
                    aux.elCara1.Fill = Brushes.FloralWhite;
                    aux.elOrejaIzq1.Fill = Brushes.FloralWhite;
                    aux.elNariz1.Fill = Brushes.FloralWhite;
                    aux.circulo.Fill = Brushes.FloralWhite;
                    aux.circulotapa.Fill = Brushes.FloralWhite;
                    aux.circulotapa.Stroke = Brushes.FloralWhite;
                    aux.circulo.Stroke = Brushes.FloralWhite;
                    aux.circulooreja.Fill = Brushes.FloralWhite;
                    aux.circulooreja1.Fill = Brushes.FloralWhite;
                    aux.circuorej.Fill = Brushes.FloralWhite;
                    aux.circulooreja1.Stroke = Brushes.FloralWhite;
                    aux.elOyuelo1.Fill = Brushes.FloralWhite;
                    aux.circuTapa2.Fill = Brushes.FloralWhite;
                    aux.circuTapa2.Stroke = Brushes.FloralWhite;
                    ///Brazos:
                    aux.brazoDer.Fill = Brushes.FloralWhite;
                    aux.manoDer.Fill = Brushes.FloralWhite;
                    aux.manoIzq.Fill = Brushes.FloralWhite;
                    aux.brazoIzq.Fill = Brushes.FloralWhite;
                    //Piernas: 
                    aux.rodillaDer.Fill = Brushes.FloralWhite;
                    aux.rodillaIzq.Fill = Brushes.FloralWhite;
                    PrecioPElo_Copy10.Content = 0;

                }
                else
                {
                    MessageBox.Show("No tiene Suficiente Dinero");
                }
                
            }
            if (VolverNormalidad.IsChecked == true)
            {
                //La cara:
                SolidColorBrush mySolidColorBrush = new SolidColorBrush();


                mySolidColorBrush.Color = (Color)ColorConverter.ConvertFromString("#FF633C20");
                MessageBox.Show("Su compra se ha realizado con exito ");
                

                aux.elCara1.Fill = mySolidColorBrush;
                aux.elOrejaIzq1.Fill = mySolidColorBrush;
                aux.elNariz1.Fill = mySolidColorBrush;
                aux.circulo.Fill = mySolidColorBrush;
                aux.circulotapa.Fill = mySolidColorBrush;
                aux.circulotapa.Stroke = mySolidColorBrush;
                aux.circulo.Stroke = mySolidColorBrush;
                aux.circulooreja.Fill = mySolidColorBrush;
                aux.circulooreja1.Fill = mySolidColorBrush;
                aux.circuorej.Fill = mySolidColorBrush;
                aux.circulooreja1.Stroke = mySolidColorBrush;
                aux.elOyuelo1.Fill = mySolidColorBrush;
                aux.circuTapa2.Fill = mySolidColorBrush;
                aux.circuTapa2.Stroke = mySolidColorBrush;
                ///Brazos:
                aux.brazoDer.Fill = mySolidColorBrush;
                aux.manoDer.Fill = mySolidColorBrush;
                aux.manoIzq.Fill = mySolidColorBrush;
                aux.brazoIzq.Fill = mySolidColorBrush;
                //Piernas: 
                aux.rodillaDer.Fill = mySolidColorBrush;
                aux.rodillaIzq.Fill = mySolidColorBrush;
            }
            this.Close();
        }

        private void VolverPeloNormal() {
            aux.elPelo1.Width = 162.691;
            aux.elPelo1.Height = 149.335;
            aux.CambiarLeft(-3.524);
            aux.CambiarTop(-2.668);
            aux.elPelo1.Fill = Brushes.Black;
        }

        public void EscribirBBDDPRecios()
        {
            //EScribiendo en la bbdd los precios. 
            OleDbConnection myConecction;
            OleDbCommand myCommand;
            OleDbCommand myCommand1;
            OleDbCommand myCommand2;
            OleDbCommand myCommand3;
            OleDbCommand myCommand4;
            OleDbCommand myCommand5;
            OleDbCommand myCommand6;
            OleDbCommand myCommand7;
            OleDbCommand myCommand8;
            OleDbCommand myCommand9;
            OleDbCommand myCommand10;
            OleDbCommand myCommand11;


            myConecction = new OleDbConnection(@"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = C:\Users\claudio jose\Desktop\Avatar Claudio\Avatar\BDCPRUEBA.accdb");
            myCommand = myConecction.CreateCommand();
            myCommand1 = myConecction.CreateCommand();
            myCommand2 = myConecction.CreateCommand();
            myCommand3 = myConecction.CreateCommand();
            myCommand4 = myConecction.CreateCommand();
            myCommand5 = myConecction.CreateCommand();
            myCommand6 = myConecction.CreateCommand();
            myCommand7 = myConecction.CreateCommand();
            myCommand8 = myConecction.CreateCommand();
            myCommand9 = myConecction.CreateCommand();
            myCommand10 = myConecction.CreateCommand();
            myCommand11 = myConecction.CreateCommand();


            String PeloSimas = Convert.ToString(PrecioPElo.Content);
            String PeloNegro = Convert.ToString(PrecioPElo_Copy2.Content);
            String PeloRojoAlargado = Convert.ToString(PrecioPElo_Copy5.Content);
            String PeinadoCuadrado = Convert.ToString(PrecioPElo_Copy.Content);
            String PeinadoEstiloAbuelo = Convert.ToString(PrecioPElo_Copy1.Content);
            String PeloAfroRubio = Convert.ToString(PrecioPElo_Copy3.Content); 
            String CamisetaRoja = Convert.ToString(PrecioPElo_Copy9.Content);
            String CamisetaRosada = Convert.ToString(PrecioPElo_Copy6.Content);
            String CamisetaMarron = Convert.ToString(PrecioPElo_Copy4.Content);
            String PantalonesAzules = Convert.ToString(PrecioPElo_Copy7.Content);
            String PantalonesAmarillo = Convert.ToString(PrecioPElo_Copy8.Content);
            String SerBlanco = Convert.ToString(PrecioPElo_Copy10.Content);

            myCommand.CommandText = "UPDATE  Peinados SET PeloSimps ='" + PeloSimas + "' WHERE Id =1";
            myCommand.CommandType = System.Data.CommandType.Text;
            myCommand1.CommandText = "UPDATE  Peinados SET PeloNegro ='" + PeloNegro + "' WHERE Id =1";
            myCommand1.CommandType = System.Data.CommandType.Text;
            myCommand2.CommandText = "UPDATE  Peinados SET PeloRojoAlargado ='" + PeloRojoAlargado + "' WHERE Id =1";
            myCommand2.CommandType = System.Data.CommandType.Text;
            myCommand3.CommandText = "UPDATE  Peinados SET Peinadocuadrado ='" + PeinadoCuadrado + "' WHERE Id =1";
            myCommand3.CommandType = System.Data.CommandType.Text;
            myCommand4.CommandText = "UPDATE  Peinados SET PeinadoEstiloAbuelo ='" + PeinadoEstiloAbuelo + "' WHERE Id =1";
            myCommand4.CommandType = System.Data.CommandType.Text;
            myCommand5.CommandText = "UPDATE  Peinados SET PeloAfroRubio ='" + PeloAfroRubio + "' WHERE Id =1";
            myCommand5.CommandType = System.Data.CommandType.Text;
            myCommand6.CommandText = "UPDATE  Peinados SET CamisetaRoja ='" + CamisetaRoja + "' WHERE Id =1";
            myCommand6.CommandType = System.Data.CommandType.Text;
            myCommand7.CommandText = "UPDATE  Peinados SET CamisetaRosada ='" + CamisetaRosada + "' WHERE Id =1";
            myCommand7.CommandType = System.Data.CommandType.Text;
            myCommand8.CommandText = "UPDATE  Peinados SET CamisetaMarron ='" + CamisetaMarron + "' WHERE Id =1";
            myCommand8.CommandType = System.Data.CommandType.Text;
            myCommand9.CommandText = "UPDATE  Peinados SET PantalonesAzulesOscuros ='" + PantalonesAzules + "' WHERE Id =1";
            myCommand9.CommandType = System.Data.CommandType.Text;
            myCommand10.CommandText = "UPDATE  Peinados SET PantalonesAmarillos ='" + PantalonesAmarillo + "' WHERE Id =1";
            myCommand10.CommandType = System.Data.CommandType.Text;
            myCommand11.CommandText = "UPDATE  Peinados SET SerBlanco ='" + SerBlanco + "' WHERE Id =1";
            myCommand11.CommandType = System.Data.CommandType.Text;

            myConecction.Open();
            try
            {
                myCommand.ExecuteNonQuery();
                myCommand1.ExecuteNonQuery();
                myCommand2.ExecuteNonQuery();
                myCommand3.ExecuteNonQuery();
                myCommand4.ExecuteNonQuery();
                myCommand5.ExecuteNonQuery();
                myCommand6.ExecuteNonQuery();
                myCommand7.ExecuteNonQuery();
                myCommand8.ExecuteNonQuery();
                myCommand9.ExecuteNonQuery();
                myCommand10.ExecuteNonQuery();
                myCommand11.ExecuteNonQuery();


            }
            catch (Exception exQuery) { }
        }

        private void VentanaTienda_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            EscribirBBDDPRecios();

        }

        public void LeerDatosBDPRecios()
        {
            OleDbConnection myConecction;
            OleDbCommand myCommand;
            myConecction = new OleDbConnection(@"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = C:\Users\claudio jose\Desktop\Avatar Claudio\Avatar\BDCPRUEBA.accdb");
            myCommand = myConecction.CreateCommand();
            myCommand.CommandText = "SELECT PeloSimps FROM Peinados WHERE id=1";
            myCommand.CommandType = System.Data.CommandType.Text;
            myConecction.Open();
            OleDbDataReader myDBReader = myCommand.ExecuteReader();
            if (myDBReader.Read())
            {
                PrecioPElo.Content = Convert.ToString(myDBReader["PeloSimps"].ToString());
            }

            OleDbCommand myCommand1;
            myCommand1 = myConecction.CreateCommand();
            myCommand1.CommandText = "SELECT PeloNegro FROM Peinados WHERE id=1";
            myCommand1.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand1.ExecuteReader();
            if (myDBReader.Read())
            {
                PrecioPElo_Copy2.Content = Convert.ToString(myDBReader["PeloNegro"].ToString());
            }
            OleDbCommand myCommand2;
            myCommand2 = myConecction.CreateCommand();
            myCommand2.CommandText = "SELECT PeloRojoAlargado FROM Peinados WHERE id=1";
            myCommand2.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand2.ExecuteReader();
            if (myDBReader.Read())
            {
                PrecioPElo_Copy5.Content = Convert.ToString(myDBReader["PeloRojoAlargado"].ToString());
            }
            OleDbCommand myCommand3;
            myCommand3 = myConecction.CreateCommand();
            myCommand3.CommandText = "SELECT Peinadocuadrado FROM Peinados WHERE id=1";
            myCommand3.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand3.ExecuteReader();
            if (myDBReader.Read())
            {
                PrecioPElo_Copy.Content = Convert.ToString(myDBReader["Peinadocuadrado"].ToString());

            }
            OleDbCommand myCommand4;
            myCommand4 = myConecction.CreateCommand();
            myCommand4.CommandText = "SELECT PeinadoEstiloAbuelo FROM Peinados WHERE id=1";
            myCommand4.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand4.ExecuteReader();
            if (myDBReader.Read())
            {
                PrecioPElo_Copy1.Content = Convert.ToString(myDBReader["PeinadoEstiloAbuelo"].ToString());


            }
            OleDbCommand myCommand5;
            myCommand5 = myConecction.CreateCommand();
            myCommand5.CommandText = "SELECT PeloAfroRubio FROM Peinados WHERE id=1";
            myCommand5.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand5.ExecuteReader();
            if (myDBReader.Read())
            {
                PrecioPElo_Copy3.Content = Convert.ToString(myDBReader["PeloAfroRubio"].ToString());

            }
            OleDbCommand myCommand6;
            myCommand6 = myConecction.CreateCommand();
            myCommand6.CommandText = "SELECT CamisetaRoja FROM Peinados WHERE id=1";
            myCommand6.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand6.ExecuteReader();
            if (myDBReader.Read())
            {
                PrecioPElo_Copy9.Content = Convert.ToString(myDBReader["CamisetaRoja"].ToString());

            }
            OleDbCommand myCommand7;
            myCommand7 = myConecction.CreateCommand();
            myCommand7.CommandText = "SELECT CamisetaMarron FROM Peinados WHERE id=1";
            myCommand7.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand7.ExecuteReader();
            if (myDBReader.Read())
            {
                PrecioPElo_Copy4.Content = Convert.ToString(myDBReader["CamisetaMarron"].ToString());

            }
            OleDbCommand myCommand8;
            myCommand8 = myConecction.CreateCommand();
            myCommand8.CommandText = "SELECT CamisetaRosada FROM Peinados WHERE id=1";
            myCommand8.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand8.ExecuteReader();
            if (myDBReader.Read())
            {
                PrecioPElo_Copy6.Content = Convert.ToString(myDBReader["CamisetaRosada"].ToString());

            }
            OleDbCommand myCommand9;
            myCommand9 = myConecction.CreateCommand();
            myCommand9.CommandText = "SELECT PantalonesAzulesOscuros FROM Peinados WHERE id=1";
            myCommand9.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand9.ExecuteReader();
            if (myDBReader.Read())
            {
                PrecioPElo_Copy7.Content = Convert.ToString(myDBReader["PantalonesAzulesOscuros"].ToString());

            }
            OleDbCommand myCommand10;
            myCommand10 = myConecction.CreateCommand();
            myCommand10.CommandText = "SELECT SerBlanco FROM Peinados WHERE id=1";
            myCommand10.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand10.ExecuteReader();
            if (myDBReader.Read())
            {
                PrecioPElo_Copy10.Content = Convert.ToString(myDBReader["SerBlanco"].ToString());

            }
            OleDbCommand myCommand11;
            myCommand11 = myConecction.CreateCommand();
            myCommand11.CommandText = "SELECT PantalonesAmarillos FROM Peinados WHERE id=1";
            myCommand11.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand11.ExecuteReader();
            if (myDBReader.Read())
            {
                PrecioPElo_Copy8.Content = Convert.ToString(myDBReader["PantalonesAmarillos"].ToString());

            }
           

        }

        public void ResetPrecios(String [] aux) {
            OleDbConnection myConecction;
            OleDbCommand myCommand;
            OleDbCommand myCommand1;
            OleDbCommand myCommand2;
            OleDbCommand myCommand3;
            OleDbCommand myCommand4;
            OleDbCommand myCommand5;
            OleDbCommand myCommand6;
            OleDbCommand myCommand7;
            OleDbCommand myCommand8;
            OleDbCommand myCommand9;
            OleDbCommand myCommand10;
            OleDbCommand myCommand11;


            myConecction = new OleDbConnection(@"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = C:\Users\claudio jose\Desktop\Avatar Claudio\Avatar\BDCPRUEBA.accdb");
            myCommand = myConecction.CreateCommand();
            myCommand1 = myConecction.CreateCommand();
            myCommand2 = myConecction.CreateCommand();
            myCommand3 = myConecction.CreateCommand();
            myCommand4 = myConecction.CreateCommand();
            myCommand5 = myConecction.CreateCommand();
            myCommand6 = myConecction.CreateCommand();
            myCommand7 = myConecction.CreateCommand();
            myCommand8 = myConecction.CreateCommand();
            myCommand9 = myConecction.CreateCommand();
            myCommand10 = myConecction.CreateCommand();
            myCommand11 = myConecction.CreateCommand();


            String PeloSimas = Convert.ToString(aux[1]);
            String PeloNegro = Convert.ToString(aux[2]);
            String PeloRojoAlargado = Convert.ToString(aux[3]);
            String PeinadoCuadrado = Convert.ToString(aux[4]);
            String PeinadoEstiloAbuelo = Convert.ToString(aux[5]);
            String PeloAfroRubio = Convert.ToString(aux[6]);
            String CamisetaRoja = Convert.ToString(aux[7]);
            String CamisetaRosada = Convert.ToString(aux[8]);
            String CamisetaMarron = Convert.ToString(aux[9]);
            String PantalonesAzules = Convert.ToString(aux[10]);
            String PantalonesAmarillo = Convert.ToString(aux[11]);
            String SerBlanco = Convert.ToString(aux[12]);

            myCommand.CommandText = "UPDATE  Peinados SET PeloSimps ='" + PeloSimas + "' WHERE Id =1";
            myCommand.CommandType = System.Data.CommandType.Text;
            myCommand1.CommandText = "UPDATE  Peinados SET PeloNegro ='" + PeloNegro + "' WHERE Id =1";
            myCommand1.CommandType = System.Data.CommandType.Text;
            myCommand2.CommandText = "UPDATE  Peinados SET PeloRojoAlargado ='" + PeloRojoAlargado + "' WHERE Id =1";
            myCommand2.CommandType = System.Data.CommandType.Text;
            myCommand3.CommandText = "UPDATE  Peinados SET Peinadocuadrado ='" + PeinadoCuadrado + "' WHERE Id =1";
            myCommand3.CommandType = System.Data.CommandType.Text;
            myCommand4.CommandText = "UPDATE  Peinados SET PeinadoEstiloAbuelo ='" + PeinadoEstiloAbuelo + "' WHERE Id =1";
            myCommand4.CommandType = System.Data.CommandType.Text;
            myCommand5.CommandText = "UPDATE  Peinados SET PeloAfroRubio ='" + PeloAfroRubio + "' WHERE Id =1";
            myCommand5.CommandType = System.Data.CommandType.Text;
            myCommand6.CommandText = "UPDATE  Peinados SET CamisetaRoja ='" + CamisetaRoja + "' WHERE Id =1";
            myCommand6.CommandType = System.Data.CommandType.Text;
            myCommand7.CommandText = "UPDATE  Peinados SET CamisetaRosada ='" + CamisetaRosada + "' WHERE Id =1";
            myCommand7.CommandType = System.Data.CommandType.Text;
            myCommand8.CommandText = "UPDATE  Peinados SET CamisetaMarron ='" + CamisetaMarron + "' WHERE Id =1";
            myCommand8.CommandType = System.Data.CommandType.Text;
            myCommand9.CommandText = "UPDATE  Peinados SET PantalonesAzulesOscuros ='" + PantalonesAzules + "' WHERE Id =1";
            myCommand9.CommandType = System.Data.CommandType.Text;
            myCommand10.CommandText = "UPDATE  Peinados SET PantalonesAmarillos ='" + PantalonesAmarillo + "' WHERE Id =1";
            myCommand10.CommandType = System.Data.CommandType.Text;
            myCommand11.CommandText = "UPDATE  Peinados SET SerBlanco ='" + SerBlanco + "' WHERE Id =1";
            myCommand11.CommandType = System.Data.CommandType.Text;

            myConecction.Open();
            try
            {
                myCommand.ExecuteNonQuery();
                myCommand1.ExecuteNonQuery();
                myCommand2.ExecuteNonQuery();
                myCommand3.ExecuteNonQuery();
                myCommand4.ExecuteNonQuery();
                myCommand5.ExecuteNonQuery();
                myCommand6.ExecuteNonQuery();
                myCommand7.ExecuteNonQuery();
                myCommand8.ExecuteNonQuery();
                myCommand9.ExecuteNonQuery();
                myCommand10.ExecuteNonQuery();
                myCommand11.ExecuteNonQuery();
                LeerDatosBDPRecios();

            }
            catch (Exception exQuery) { }
        }

        public String [] getAux1() {
            return aux1;
        }
    }


}
