﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Avatar
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer t1; //Reloj para las barras de tiempo. 
        DispatcherTimer t2; //Reloj para el tiempo Jugado.

        int dinero = 0;
        int experiencia = 0;
        int contadorLevel = 1;
        double seg = 0;
        double min = 0;
        double horas = 0;
        Boolean logros = false;
        Window1 TiendaAvatar;
        Boolean logro1 = false;
        Boolean logro2 = false;
        Boolean logro3 = false;
        Boolean logro4 = false;
        Boolean logro5 = false;
        Boolean logro6 = false;
        int contadorComida = 0;
        int contadorDespertar = 0;
        String[] preciosPeinados = new String [13];


        public int getDinero()
        {
            return dinero;
        }
        public void setDinero(int nuevoDinero)
        {
            dinero = nuevoDinero;
        }

        public MainWindow()
        {
            ///Esto es para controlar el temporizador.
            InitializeComponent();
            //TiendaAvatar = new Window1(this);

            Class1 avatar = new Class1(100, 100, 100);
            //InitializeComponent();
            t1 = new DispatcherTimer();
            t1.Interval = TimeSpan.FromSeconds(1.0);
            t1.Tick += new EventHandler(reloj);
            LeerDatosBD();
            SelectorNivel.Items.Add("1");
            SelectorNivel.Items.Add("2");
            SelectorNivel.Items.Add("3");
            SelectorNivel.SelectedIndex = 0;
            t1.Start();
            t2 = new DispatcherTimer();
            t2.Interval = TimeSpan.FromSeconds(1.0);
            t2.Tick += new EventHandler(reloj1);
            t2.Start();
        }
        ///
        private void LeerDatosBD()
        {
            InitializeComponent();
            OleDbConnection myConecction;
            OleDbCommand myCommand;
            myConecction = new OleDbConnection(@"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = C:\Users\claudio jose\Desktop\Avatar Claudio\Avatar\BDCPRUEBA.accdb");
            myCommand = myConecction.CreateCommand();
            myCommand.CommandText = "SELECT Energia FROM Atributos WHERE id=1";
            myCommand.CommandType = System.Data.CommandType.Text;
            myConecction.Open();
            OleDbDataReader myDBReader = myCommand.ExecuteReader();
            if (myDBReader.Read())
            {
                pbSueño.Value = Convert.ToDouble(myDBReader["Energia"].ToString());
            }
            OleDbCommand myCommand1;
            myCommand1 = myConecction.CreateCommand();
            myCommand1.CommandText = "SELECT Comida FROM Atributos WHERE id=1";
            myCommand1.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand1.ExecuteReader();
            if (myDBReader.Read())
            {
                pbApetito.Value = Convert.ToDouble(myDBReader["Comida"].ToString());
            }
            OleDbCommand myCommand2;
            myCommand2 = myConecction.CreateCommand();
            myCommand2.CommandText = "SELECT Diversion FROM Atributos WHERE id=1";
            myCommand2.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand2.ExecuteReader();
            if (myDBReader.Read())
            {
                pbJugar.Value = Convert.ToDouble(myDBReader["Diversion"].ToString());
            }
            OleDbCommand myCommand3;
            myCommand3 = myConecction.CreateCommand();
            myCommand3.CommandText = "SELECT Monedas FROM Atributos WHERE id=1";
            myCommand3.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand3.ExecuteReader();
            if (myDBReader.Read())
            {
                dinero = Convert.ToInt32(myDBReader["Monedas"].ToString());
                nMonedas.Content = Convert.ToString(dinero);

            }
            OleDbCommand myCommand4;
            myCommand4 = myConecction.CreateCommand();
            myCommand4.CommandText = "SELECT TiempoJuego FROM Atributos WHERE id=1";
            myCommand4.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand4.ExecuteReader();
            if (myDBReader.Read())
            {
                txtTiempo.Text = Convert.ToString(myDBReader["TiempoJuego"].ToString());
                List<String> numeros = txtTiempo.Text.Split(':').ToList<String>();
                seg = Convert.ToInt32(numeros[2]);
                min = Convert.ToInt32(numeros[1]);
                horas = Convert.ToInt32(numeros[0]);

            }
            OleDbCommand myCommand5;
            myCommand5 = myConecction.CreateCommand();
            myCommand5.CommandText = "SELECT Experiencia FROM Atributos WHERE id=1";
            myCommand5.CommandType = System.Data.CommandType.Text;
            myDBReader = myCommand5.ExecuteReader();
            if (myDBReader.Read())
            {
                experiencia = Convert.ToInt32(myDBReader["Experiencia"].ToString());
                contadorLevel = experiencia;
                NivelExpText.Text = Convert.ToString(experiencia);
            }

        }
        ///En este metodo se controlan las progressbar.

        private void reloj(Object sender, EventArgs e)
        {
            pbApetito.Value -= 5.0;
            pbJugar.Value -= 2.0;
            pbSueño.Value -= 5.0;
            Storyboard sbdormir = (Storyboard)this.Resources["MostrarZ"];
            barrasSueño(sbdormir);
            Storyboard sbcomer = (Storyboard)this.Resources["Comer"];
            barrasComer(sbcomer);
            
        }
        private void reloj1(Object sender, EventArgs e)
        {
            tiempoJugado();
        }

        private void btnComer_Click(object sender, RoutedEventArgs e)
        {
            pbApetito.Value += 10;
            Storyboard sbcomer = (Storyboard)this.Resources["Comer"];
            NubeComida.Opacity = 0;
            Storyboard sbNubeComida = (Storyboard)this.Resources["NubeComidaStory"];
            contadorComida += 1;

            if (pbApetito.Value < 50)
            {
                barriga.Margin = new Thickness(0, 0, 0, 0);
                barriga.Width = 74.716;
                barriga.Height = 61.179;
                //Left: 37,397
                //Top: 144,43
                sbcomer.Begin();
                sbNubeComida.Stop();
            }
            else if (pbApetito.Value <= 65)
            {
                barriga.Width = 88.666;
                barriga.Height = 74.173;
                //Left: 31,333
                //Top: 140,222

                barriga.Margin = new Thickness(-10, -10, 0, 0);
                sbcomer.Begin();
            }
            else if (pbApetito.Value <= 80)
            {
                barriga.Width = 100.666;
                barriga.Height = 81.173;
                //Left: 25,583
                //Top: 138,722
                sbcomer.Begin();

            }
            else if (pbApetito.Value <= 100)
            {
                barriga.Width = 100.166;
                barriga.Height = 87.173;
                //Left: 20,583
                //Top: 136,722
                sbcomer.Begin();
            }
        }

        private void btnDormir_Click(object sender, RoutedEventArgs e)
        {
            pbSueño.Value += 20;
            contadorDespertar += 1;
        }

        private void btnJugar_Click(object sender, RoutedEventArgs e)
        {
            Storyboard sbJugar = (Storyboard)this.Resources["Jugar"];
            Storyboard sbdormir = (Storyboard)this.Resources["MostrarZ"];
            Storyboard sbcomer = (Storyboard)this.Resources["Comer"];
            sbcomer.Stop();
            sbdormir.Stop();
            sbJugar.Begin();

        }

        private void barrasSueño(Storyboard sbdormir)
        {
            btnAlimentar.IsEnabled = true;
            btnJugar.IsEnabled = true;
            Storyboard sbNubeComida = (Storyboard)this.Resources["NubeComidaStory"];
            if (pbSueño.Value > 50 && pbApetito.Value > 80)
            {
                elOjoDer1_Copy1.Opacity = 100;
                elOjoDer1_Copy2.Opacity = 100;
                pupilaDer.Opacity = 100;
                rayaDer.Opacity = 100;
                rayaIzq.Opacity = 100;
                pupilaIzq.Opacity = 100;
                pupilaIzq.Height = 11.234;
                pupilaDer.Height = 11.234;
                NubeComida.Opacity = 0;
            }
            else if (pbSueño.Value >= 50)
            {

                elOjoDer1_Copy1.Opacity = 0;
                elOjoDer1_Copy2.Opacity = 0;
                rayaDer.Opacity = 0;
                rayaIzq.Opacity = 0;
                sbdormir.Stop();
                pupilaIzq.Height = 11.234;
                pupilaDer.Height = 11.234;
            }

            else if (pbSueño.Value < 15)
            {
                pupilaIzq.Height = 1.234;
                pupilaDer.Height = 1.234;
                parpadoIzq.Height = 1.234;
                parpadoDer.Height = 1.234;
                sbdormir.Begin();
                sbNubeComida.Stop();
                btnAlimentar.IsEnabled = false;
                btnJugar.IsEnabled = false;
                Dormir.Opacity = 100;
                NubeComida.Opacity = 0;
                elOjoDer1_Copy1.Opacity = 0;
                elOjoDer1_Copy2.Opacity = 0;
                rayaIzq.Opacity = 0;
                rayaDer.Opacity = 0;

            }

            else if (pbSueño.Value > 15 && pbSueño.Value < 50)
            {
                pupilaIzq.Height = 4;
                pupilaDer.Height = 4;
                parpadoIzq.Height = 3;
                parpadoDer.Height = 3;
                sbdormir.Stop();
                sbNubeComida.Stop();
                NubeComida.Opacity = 0;
                elOjoDer1_Copy1.Opacity = 0;
                elOjoDer1_Copy2.Opacity = 0;
                rayaIzq.Opacity = 0;
                rayaDer.Opacity = 0;

            }
            //ESTA NO SE EJECUTA 
            else if (pbSueño.Value < 50)
            {
                elOjoDer1_Copy1.Opacity = 0;
                elOjoDer1_Copy2.Opacity = 0;
                pupilaIzq.Height = 8;
                pupilaDer.Height = 8;
                NubeComida.Opacity = 0;
                               
            }
        }
        private void barrasComer(Storyboard sbcomer)
        {
            if (pbApetito.Value < 50 && pbApetito.Value > 15)
            {
                Storyboard sbNubeComida = (Storyboard)this.Resources["NubeComidaStory"];
                sbNubeComida.Begin();
            }
            if (pbApetito.Value < 50)
            {
                barriga.Margin = new Thickness(0, 0, 0, 0);
                barriga.Width = 74.716;
                barriga.Height = 61.179;
            }

            else if (pbApetito.Value <= 65)
            {
                barriga.Width = 88.666;
                barriga.Height = 74.173;

                barriga.Margin = new Thickness(-10, -10, 0, 0);
            }
            else if (pbApetito.Value <= 80)
            {
                barriga.Width = 100.666;
                barriga.Height = 81.173;

            }
            else if (pbApetito.Value <= 100)
            {
                barriga.Width = 100.166;
                barriga.Height = 87.173;
            }
            else if (pbSueño.Value <= 15)
            {
                Storyboard sbNubeComida = (Storyboard)this.Resources["NubeComidaStory"];
                sbNubeComida.Stop();
            }

        }

        private void elPelo1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Storyboard sbJugar = (Storyboard)this.Resources["Jugar"];
            sbJugar.Stop();
            pbJugar.Value += 100;
            dinero = dinero + 10;
            nMonedas.Content = Convert.ToString(dinero);
        }

        private void btplay_Click(object sender, RoutedEventArgs e)
        {
            NubeComida.Opacity = 0;

            if (pbSueño.Value >= 30 && pbApetito.Value >= 30)
            {
                t1.Stop();

                if (SelectorNivel.SelectedItem.Equals("1"))
                {
                    Cleveland.Opacity = 100;
                    Storyboard sbcleveland = (Storyboard)this.Resources["ClevelandBailar"];
                    sbcleveland.Begin();
                    Storyboard sbNivel1 = (Storyboard)this.Resources["Nivel1"];
                    sbNivel1.BeginTime = TimeSpan.FromSeconds(2.7);
                    Pelos.Opacity = 100;
                    sbNivel1.Begin();
                }
                else if (SelectorNivel.SelectedItem.Equals("2"))
                {
                    Cleveland.Opacity = 100;
                    Storyboard sbcleveland = (Storyboard)this.Resources["ClevelandBailar"];
                    sbcleveland.Begin();
                    Storyboard sbNivel2 = (Storyboard)this.Resources["Nivel2"];
                    sbNivel2.BeginTime = TimeSpan.FromSeconds(2.7);
                    Pelos.Opacity = 100;
                    sbNivel2.Begin();

                }
                else if (SelectorNivel.SelectedItem.Equals("3"))
                {
                    Cleveland.Opacity = 100;
                    Storyboard sbcleveland = (Storyboard)this.Resources["ClevelandBailar"];
                    sbcleveland.Begin();
                    Storyboard sbNivel3 = (Storyboard)this.Resources["Nivel3"];
                    sbNivel3.BeginTime = TimeSpan.FromSeconds(2.7);
                    Pelos.Opacity = 100;
                    sbNivel3.Begin();
                }
            }
            else
            {
                MessageBox.Show("No tienes suficiente energia o alimento para Jugar al Extremo");
            }
            // t1.Stop();

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Quieres Guardar la partida ?", "Confirmación", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                //EScribiendo en la bbdd
                OleDbConnection myConecction;
                OleDbCommand myCommand;
                OleDbCommand myCommand1;
                OleDbCommand myCommand2;
                OleDbCommand myCommand3;
                OleDbCommand myCommand4;
                OleDbCommand myCommand5;
                
                myConecction = new OleDbConnection(@"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = C:\Users\claudio jose\Desktop\Avatar Claudio\Avatar\BDCPRUEBA.accdb");
                myCommand = myConecction.CreateCommand();
                myCommand1 = myConecction.CreateCommand();
                myCommand2 = myConecction.CreateCommand();
                myCommand3 = myConecction.CreateCommand();
                myCommand4 = myConecction.CreateCommand();
                myCommand5 = myConecction.CreateCommand();

                double jugar = pbJugar.Value;
                String jugar1 = Convert.ToString(jugar);
                double comer = pbApetito.Value;
                String comer1 = Convert.ToString(comer);
                double dormir = pbSueño.Value;
                String dormir1 = Convert.ToString(dormir);
                String monedas = Convert.ToString(nMonedas.Content);
                String exp = Convert.ToString(NivelExpText.Text);

                myCommand.CommandText = "UPDATE  Atributos SET Energia ='" + jugar1 + "' WHERE Id =1";
                myCommand.CommandType = System.Data.CommandType.Text;
                myCommand1.CommandText = "UPDATE  Atributos SET Comida ='" + comer1 + "' WHERE Id =1";
                myCommand1.CommandType = System.Data.CommandType.Text;
                myCommand2.CommandText = "UPDATE  Atributos SET Diversion ='" + dormir1 + "' WHERE Id =1";
                myCommand2.CommandType = System.Data.CommandType.Text;
                myCommand3.CommandText = "UPDATE  Atributos SET Monedas ='" + monedas + "' WHERE Id =1";
                myCommand3.CommandType = System.Data.CommandType.Text;
                myCommand4.CommandText = "UPDATE  Atributos SET TiempoJuego ='" + txtTiempo.Text + "' WHERE Id =1";
                myCommand4.CommandType = System.Data.CommandType.Text;
                myCommand5.CommandText = "UPDATE  Atributos SET Experiencia ='" + exp + "' WHERE Id =1";
                myCommand5.CommandType = System.Data.CommandType.Text;
                myConecction.Open();

                try
                {
                    myCommand.ExecuteNonQuery();
                    myCommand1.ExecuteNonQuery();
                    myCommand2.ExecuteNonQuery();
                    myCommand3.ExecuteNonQuery();
                    myCommand4.ExecuteNonQuery();
                    myCommand5.ExecuteNonQuery();
                    TiendaAvatar.EscribirBBDDPRecios();
                }
                catch (Exception exQuery) { }


            }
        }

        private double Dado(int v)
        {
            Random dado = new Random();
            return 1 + dado.Next(v);
        }
        private void tiempoJugado()
        {
            //cada 7 segundos dan 1 moneda, al minuto 10

            seg += this.Dado(1);
            if (seg % 1 == 0)
            {
                nMonedas.Content = Convert.ToString(dinero);
            }
            if (seg % 7 == 0)
            {
                dinero += 10;
                nMonedas.Content = Convert.ToString(dinero);
            }
            if (seg == 60)
            {
                seg = 0;
                min++;
                nMonedas.Content = Convert.ToString(dinero);
            }
            if (min == 60)
            {
                min = 0;
                horas++;
            }

            txtTiempo.Text = Convert.ToString(horas) + ":" + Convert.ToString(min) + ":" + Convert.ToString(seg);
            ComprobarLogros();
            ActualizarLevel();

        }

        private void PeloNivel1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Storyboard sbNivel1 = (Storyboard)this.Resources["Nivel1"];
            sbNivel1.Stop();
            Storyboard sbNivel2 = (Storyboard)this.Resources["Nivel2"];
            sbNivel2.Stop();
            Storyboard sbNivel3 = (Storyboard)this.Resources["Nivel3"];
            sbNivel3.Stop();
            Pelos.Opacity = 0;
            t1.Start();
            dinero += 50;
            experiencia += 100;


        }

        private void PeloNivel2_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Storyboard sbNivel1 = (Storyboard)this.Resources["Nivel1"];
            sbNivel1.Stop();
            Storyboard sbNivel3 = (Storyboard)this.Resources["Nivel3"];
            sbNivel3.Stop();
            Storyboard sbNivel2 = (Storyboard)this.Resources["Nivel2"];
            sbNivel2.Stop();
            Pelos.Opacity = 0;
            t1.Start();
            dinero += 100;
            experiencia += 100;

        }

        private void PeloNivel3_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Storyboard sbNivel1 = (Storyboard)this.Resources["Nivel1"];
            sbNivel1.Stop();
            Storyboard sbNivel2 = (Storyboard)this.Resources["Nivel2"];
            sbNivel2.Stop();
            Storyboard sbNivel3 = (Storyboard)this.Resources["Nivel3"];
            sbNivel3.Stop();
            Pelos.Opacity = 0;
            t1.Start();
            dinero += 300;
            experiencia += 100;

        }

        private void TiendaBoton_Click(object sender, RoutedEventArgs e)
        {
            TiendaAvatar = new Window1(this);
                            
            //Comprobacion logros de nivel.
            if (logros == true) {
                TiendaAvatar.PrecioPElo_Copy6.Content = 0;
            }
            if (logro1 == true)
            {
                TiendaAvatar.PrecioPElo_Copy8.Content = 0;
            }
            if (logro2 == true)
            {
                TiendaAvatar.PrecioPElo_Copy10.Content = 0;
            }

            //Logros comida:

            if (logro4 == true)
            {
                TiendaAvatar.PrecioPElo_Copy5.Content = 0;
            }

            ///logro despertarse:
            if (logro6 == true)
            {
                TiendaAvatar.PrecioPElo_Copy7.Content = 0;
            }
            
            TiendaAvatar.Show();

        }

        private void LogrosAccion()
        {
            Storyboard Logros = (Storyboard)this.Resources["Logros"];
            Logros.Begin();
        }
        public void CambiarTop(double nuevoTop)
        {
            Canvas.SetTop(elPelo1, nuevoTop);
        }
        public void CambiarLeft(double nuevoLeft)
        {
            Canvas.SetLeft(elPelo1, nuevoLeft);
        }

        public void ApagarBotones()
        {
            btnAlimentar.IsEnabled = false;
            btnJugar.IsEnabled = false;
            btnDormir.IsEnabled = false;
            BtnTienda.IsEnabled = false;
            btplay.IsEnabled = false;

        }

        private void EncenderBotones()
        {
            btnAlimentar.IsEnabled = true;
            btnJugar.IsEnabled = true;
            btnDormir.IsEnabled = true;
            BtnTienda.IsEnabled = true;
            btplay.IsEnabled = true;

        }

        private void NivelExp_MouseDown(object sender, MouseButtonEventArgs e)
        {
            LogrosAccion();
        }

        private void NivelExpText_MouseDown(object sender, MouseButtonEventArgs e)
        {
            LogrosAccion();

        }

        public void ComprobarLogros()
        {

            int aux = Convert.ToInt32(NivelExpText.Text);

            //////////////////Logros de level!!!/////////////
            if (aux == 5 && logros == false)
            {
                LogroText.Text = "Has Conseguido el Logro del Nivel 5!!!!!";
                Storyboard Logros = (Storyboard)this.Resources["LogroConseguido!"];
                logros = true;
                Logros.Begin();
                CambiarEstadoLogro(DibujoX_Copy2);
            }
            if (aux == 10 && logro1 == false)
            {
                LogroText.Text = "Has Conseguido el Logro del Nivel 10!!!!!";
                Storyboard Logros = (Storyboard)this.Resources["LogroConseguido!"];
                logro1 = true;
                Logros.Begin();
                dinero += 500;
                CambiarEstadoLogro(DibujoX_Copy1);
            }
            if (aux == 15 && logro2 == false)
            {
                LogroText.Text = "Has Conseguido el Logro del Nivel 15!!!!!";
                Storyboard Logros = (Storyboard)this.Resources["LogroConseguido!"];
                logro2 = true;
                dinero += 1000;
                Logros.Begin();
                CambiarEstadoLogro(DibujoX_Copy);
            }
                        //////////////////Logros sobre Comida////////////////
            if (contadorComida >= 10 && logro3 == false)
            {
                LogroText.Text = "Has Conseguido el Logro de Comer 10 Veces!!!!!";
                Storyboard Logros = (Storyboard)this.Resources["LogroConseguido!"];
                logro3 = true;
                Logros.Begin();
                Thread.Sleep(3);
                dinero += 500;
                CambiarEstadoLogro(DibujoX_Copy3);

            }
            if (contadorComida >= 20 && logro4 == false)
            {
                LogroText.Text = "Has Conseguido el Logro de Comer 20 Veces!!!!!";
                Storyboard Logros = (Storyboard)this.Resources["LogroConseguido!"];
                logro4 = true;
                Logros.Begin();
                dinero += 1000;
                CambiarEstadoLogro(DibujoX_Copy4);
            }


            //////////////////Logros sobre juegos///////////////
            //////////////////Logros sobre Despertarse///////////
            if (contadorDespertar >= 10 && logro5 == false)
            {
                LogroText.Text = "Has Conseguido el Logro de Despertarse 10 Veces!!!!!";
                Storyboard Logros = (Storyboard)this.Resources["LogroConseguido!"];
                logro5 = true;
                Logros.Begin();
                Thread.Sleep(3);
                dinero += 500;
                CambiarEstadoLogro(DibujoX_Copy5);
            }
            if (contadorDespertar >= 20 && logro6 == false)
            {
                LogroText.Text = "Has Conseguido el Logro de Despertarse 20 Veces!!!!!";
                Storyboard Logros = (Storyboard)this.Resources["LogroConseguido!"];
                logro6 = true;
                Logros.Begin();
                experiencia += 100;
                CambiarEstadoLogro(DibujoX_Copy6);
            }

            ///////////////////////////////////////////////////////
            if (seg % 7 == 0)
            {
                Storyboard Logros = (Storyboard)this.Resources["LogroConseguido!"];
                Logros.Stop();
            }
        }
        private void ActualizarLevel()
        {

            if (experiencia >= 100)
            {
                contadorLevel += 1;
                experiencia = 0;
            }
            NivelExpText.Text = Convert.ToString(contadorLevel);
        }

        private void Logros_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Storyboard Logros = (Storyboard)this.Resources["Logros"];
            Logros.Stop();
        }

        private void NivelExpLogro_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //Storyboard Logros = (Storyboard)this.Resources["LogroConseguido!"];
            //Logros.Stop();
        }

        private void CambiarEstadoLogro(Label aux) {
            ImageBrush myBrush = new ImageBrush();
            Image image = new Image();
            image.Source = new BitmapImage(
                new Uri(AppDomain.CurrentDomain.BaseDirectory + "BienEcho2.png"));
            myBrush.ImageSource = image.Source;
            aux.Background = myBrush;
            
        }
        private void CambiarEstadoLogroFallido(Label aux)
        {
            ImageBrush myBrush = new ImageBrush();
            Image image = new Image();
            image.Source = new BitmapImage(
                new Uri(AppDomain.CurrentDomain.BaseDirectory + "MalEcho.png"));
            myBrush.ImageSource = image.Source;
            aux.Background = myBrush;

        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Seguro que quieres volver a empezar la partida ?", "Confirmación", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes){
                TiendaAvatar = new Window1(this);
                preciosPeinados = TiendaAvatar.getAux1();
                TiendaAvatar.ResetPrecios(preciosPeinados);
                ResetAll();
                ResetearLogros();
                LeerDatosBD();
                TiendaAvatar.Close();

            }                
        }

        private void ResetAll() {
            //Implementar metodo. 
            OleDbConnection myConecction;
            OleDbCommand myCommand;
            OleDbCommand myCommand1;
            OleDbCommand myCommand2;
            OleDbCommand myCommand3;
            OleDbCommand myCommand4;
            OleDbCommand myCommand5;

            myConecction = new OleDbConnection(@"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = C:\Users\claudio jose\Desktop\Avatar Claudio\Avatar\BDCPRUEBA.accdb");
            myCommand = myConecction.CreateCommand();
            myCommand1 = myConecction.CreateCommand();
            myCommand2 = myConecction.CreateCommand();
            myCommand3 = myConecction.CreateCommand();
            myCommand4 = myConecction.CreateCommand();
            myCommand5 = myConecction.CreateCommand();

            
            String jugar1 = Convert.ToString(100);            
            String comer1 = Convert.ToString(0);            
            String dormir1 = Convert.ToString(100);
            String monedas = Convert.ToString(0);
            String exp = Convert.ToString(0);
            String tiempo =  Convert.ToString(0) + ":" + Convert.ToString(0) + ":" + Convert.ToString(0);


            myCommand.CommandText = "UPDATE  Atributos SET Energia ='" + jugar1 + "' WHERE Id =1";
            myCommand.CommandType = System.Data.CommandType.Text;
            myCommand1.CommandText = "UPDATE  Atributos SET Comida ='" + comer1 + "' WHERE Id =1";
            myCommand1.CommandType = System.Data.CommandType.Text;
            myCommand2.CommandText = "UPDATE  Atributos SET Diversion ='" + dormir1 + "' WHERE Id =1";
            myCommand2.CommandType = System.Data.CommandType.Text;
            myCommand3.CommandText = "UPDATE  Atributos SET Monedas ='" + monedas + "' WHERE Id =1";
            myCommand3.CommandType = System.Data.CommandType.Text;
            myCommand4.CommandText = "UPDATE  Atributos SET TiempoJuego ='" + tiempo + "' WHERE Id =1";
            myCommand4.CommandType = System.Data.CommandType.Text;
            myCommand5.CommandText = "UPDATE  Atributos SET Experiencia ='" + exp + "' WHERE Id =1";
            myCommand5.CommandType = System.Data.CommandType.Text;
            myConecction.Open();

            try
            {
                myCommand.ExecuteNonQuery();
                myCommand1.ExecuteNonQuery();
                myCommand2.ExecuteNonQuery();
                myCommand3.ExecuteNonQuery();
                myCommand4.ExecuteNonQuery();
                myCommand5.ExecuteNonQuery();
                TiendaAvatar.EscribirBBDDPRecios();
            }
            catch (Exception exQuery) { }

        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            Storyboard Ayuda = (Storyboard)this.Resources["Ayuda"];
            Ayuda.Begin();
        }

        private void ResetearLogros() {
            logros = false;
            logro1 = false;
            logro2 = false;
            logro3 = false;
            logro4 = false;
            logro5 = false;
            logro6 = false;
            contadorComida = 0;
            contadorDespertar = 0;
            CambiarEstadoLogroFallido(DibujoX_Copy2);
            CambiarEstadoLogroFallido(DibujoX_Copy1);
            CambiarEstadoLogroFallido(DibujoX_Copy);
            CambiarEstadoLogroFallido(DibujoX);
            CambiarEstadoLogroFallido(DibujoX1);
            CambiarEstadoLogroFallido(DibujoX_Copy3);
            CambiarEstadoLogroFallido(DibujoX_Copy4);
            CambiarEstadoLogroFallido(DibujoX_Copy5);
            CambiarEstadoLogroFallido(DibujoX_Copy6);
        }
        private void Ayuda_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Storyboard Ayuda = (Storyboard)this.Resources["Ayuda"];
            Ayuda.Stop();
        }
    }
}